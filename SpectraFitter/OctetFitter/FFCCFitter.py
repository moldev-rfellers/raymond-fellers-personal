# Use direct five frame cross-correlation (FFCC) to find the peak shift
#
import sys
sys.path.append(r'C:/Users/ForteBio/Documents/BitBucket/raymond-fellers-personal/SpectraFitter')
import numpy as np
import OctetUtilities.TransmissionData as td
import OctetUtilities.Filters as flt
import OctetFitter.FFCCMatrix as ccmat
import matplotlib.pyplot as plt
import scipy.signal
from scipy.optimize import curve_fit

class FFCCFitter():
    def __init__(self):
        # Input transmission curves and normalized transmission curves
        self.td = []
        self.ntd = []
        
        # wavelengths
        self.wl = []        # Wavelengths
        
        # Shifts before optimizing with matrix
        self.pyOctsh = []
        
        # Save the nearest index of the peak maximum
        # maxIdx is the index of the last processed trace used for
        # relative frequency calculation.
        self.maxIdx = 0
        
    def getSensorgram(self, spectraFile):
        self.to = td.TransmissionData() 
        (self.td,self.wl) = self.to.makeTransmissionDataFromSpectraFile(spectraFile)
        self.normalizeAllTransmissions()
        self.computeShifts()
        (time, nm) = self.optimizeShifts()
        return (time, nm)
    
    def getLength(self):
        return len(self.ntd)
    
    def getTransmission(self, i):
        return self.ntd[i][1]
    
    def normalizeAllTransmissions(self):
        self.ntd = self.td
        for i in range(len(self.td)):
            self.ntd[i][1] = self.normalizeTransmission(self.td[i][1])
            
    def computeShifts(self):
        cnt=0
        self.pyOctsh = []
        for j in range(self.getLength()):
            # Compute the shift for the 5 previous frames if we have that
            # many, or use what we have is there are less at the beginning
            start = 0
            if(j > 5):
                start = j-5
            denom = 1
            for i in reversed(range(start,j)):
                veci = self.getTransmission(i)
                vecj = self.getTransmission(j)
                r = self.shift(veci, vecj)
                cnt = cnt+1
                self.pyOctsh.append(r/denom)
                denom = denom+1
                    
    def optimizeShifts(self):
        ffmat = ccmat.FFCCMatrix()
        shiftTemp = []
        # Zero pad the array so that at the end, the last slice of pyOctsh is the same length
        # of the matrix row we take the dot product.
        #for i in range(15):
        #    self.pyOctsh.append(0.0)
        lpo = len(self.pyOctsh)
            
        # Take the first vector of shifts and take the dot product against the top half of
        # of the transform matrix.  We can only do this when we have accumulated at least 30
        # shift measurements so that we can use a slice of pyOctsh that has the right length.
        for i in range(0,16):
            shiftTemp.append( np.dot(self.pyOctsh[0:145], ffmat.getRow(31,i)) )
            
        # In the middle we take advantage of the periodicity of the transform matrix
        # (see Lian's slides) and use the same middle row repeatedly.  The change here is
        # each dot product is against a slightly different slice of pyOctsh representing
        # an evolution in time.
        for i in range(5, lpo-145+5, 5):
            shiftTemp.append( np.dot(self.pyOctsh[i:i+145], ffmat.getRow(31,15)) )
            
        # And near the end we reuse a single slice of pyOctsh and take the dot product against
        # the rows of the lower half of the transformation matrix.  This is where we need the
        # zero pad because the spectra recording seldom stops at just the right time to make
        # the slice size match the matrix dimension.
        for i in range(16,31):
            shiftTemp.append( np.dot(self.pyOctsh[lpo-145:], ffmat.getRow(31,i)) )
            
        # Now we have our new sets of shifts, construct a running sum to get the sensogram.
        # Note at this point we are still in pixel shift units.
        shifts = []
        shifts.append(shiftTemp[0])
        for i in range(1,len(shiftTemp)):
            shifts.append( shifts[-1] + shiftTemp[i] )
            
        (timestamps, nmShifts) = self.pixelToRelativeNanoMeter(shifts)
        #(timestamps, nmShifts) = self.pixelToAbsoluteNanoMeter(shifts)
        return (timestamps, nmShifts)

 
    # This reproduces how the Octet code takes the pixel shifts and converts
    # it into a sensorgram with relative nm shfit on the y-axis.
    def pixelToRelativeNanoMeter(self, shifts):
        timestamps = []
        p2nm = 0.0
        shiftFactor = (self.wl[-1] - self.wl[0])/len(self.wl)
        p2nm = shiftFactor*(self.wl[self.maxIdx]/(shiftFactor*self.maxIdx + self.wl[0]))
        for i in range(len(shifts)):
            shifts[i] = shifts[i] * p2nm
        shifts.insert(0, 0.0)
        for i in range(0,len(self.td)):
            timestamps.append(self.td[i][0]/1000.0)
        return (timestamps, shifts)
    
    # An extension of pixelToRelativeNanoMeter() but in here the shifts
    # array are absolute nm.  To get the starting nm wavelength we fit
    # the peak center of the first trace to a gaussian for sub-pixel
    # resolution and use a SciPy spline interpolation package to get
    # the wavelength.
    #
    def pixelToAbsoluteNanoMeter(self, shifts):
        _,_,_,coeff = self.FitGaussian(self.ntd[0][1])
        startnm = self.to.getWavelength(coeff[1])

        timestamps = []
        p2nm = 0.0
        shiftFactor = (self.wl[-1] - self.wl[0])/len(self.wl)
        p2nm = shiftFactor*(self.wl[self.maxIdx]/(shiftFactor*self.maxIdx + self.wl[0]))
        for i in range(len(shifts)):
            shifts[i] = shifts[i] * p2nm + startnm
        shifts.insert(0, startnm)
        for i in range(0,len(self.td)):
            timestamps.append(self.td[i][0]/1000.0)
        return (timestamps, shifts)
        
    
    # Prune the transmission trace so that it is ready
    # for cross-correlation.  This routine prepares the
    # trace exactly as done in the Octet code.  Some of 
    # settings are not explain and appear to be determined
    # by trial and error to achieve the best result with
    # the data available.
    #
    # This method works on a single trace.
    def normalizeTransmission(self, trace):
        # Set the first 150 and last 200 points to zero
        trace[:150] = [0.0] * 150
        trace[-200:] = [0.0] * 200
        
        # Smooth out the trace with a flat moving average.
        smTrace = flt.octet_smooth(trace, window_len=21, window='flat')
        
        # Find the maximum of the trace and the local minima on either side.
        #
        # The search on the left begins at the part after zeroing plus the moving
        # average window width, 150 + 21 = 171 is the starting index.
        #
        # On the right side the search stops at the 85% or index 3684*85% = 3100.
        #
        
        # Find the global max.
        # numpy.where returns an array of all matches. We only need the first one.
        maxVal = np.amax(smTrace)
        maxIdxArr = np.where(smTrace == maxVal)
        maxIdx = int(maxIdxArr[0])
        self.maxIdx = maxIdx
        
        # Look for the local minimum to the right of the max.
        # Using a slice to restrict the search region means the return index is the
        # index for that slice so we need to add the max index to get the minimum
        # for the full trace.
        minRightVal = np.amin(smTrace[maxIdx:3100])
        minRightArr = np.where(smTrace[maxIdx:3100] == minRightVal)
        minRightIdx = int(minRightArr[0]+maxIdx)
        
        # Now the minimum on the left
        minLeftVal = np.amin(smTrace[171:maxIdx])
        minLeftArr = np.where(smTrace[171:maxIdx] == minLeftVal)
        minLeftIdx = int(minLeftArr[0] + 171)
        
        # Normalize
        # Take the greater of the two minima above and subtract them from every element
        cutoff = minRightVal
        if(minLeftVal > minRightVal):
            cutoff = minLeftVal    
        nTrace = np.subtract(smTrace, cutoff)
        
        # Anything to the left or right of the minima set to zero
        nTrace[minRightIdx:] = [0.0] * (len(nTrace)-minRightIdx)
        nTrace[:minLeftIdx] = [0.0] * (minLeftIdx)
        
        # Anything left that is below 0 set to 0
        nTrace = nTrace.clip(min=0.0)
        return nTrace
    
    # This is (almost) a direct port of CBase::shift()
    #
    def shift(self, veci, vecj):
        
        sum0 = np.dot(veci, np.roll(vecj, 0))
        sum1 = np.dot(veci, np.roll(vecj, -1))
        sum2 = 0
        
        # Complete the remainder of Shift() with an almost direct
        # translation into Python
        sum2 = 0
        iMax = 0
        i = 1
        if(sum0 > sum1):
            sum2 = sum1
            sum1 = sum0
            i = -1
            sum0 = np.dot(veci, np.roll(vecj, -i))
            while(sum0 > sum1):
                sum2 = sum1
                sum1 = sum0
                i = i-1
                sum0 = np.dot(veci, np.roll(vecj, -i))
            iMax = i+1
        else:
            i = 2
            sum2 = np.dot(veci, np.roll(vecj, -i))
            while(sum2 > sum1):
                sum0 = sum1
                sum1 = sum2
                i = i+1
                sum2 = np.dot(veci, np.roll(vecj, -i))
            iMax = i-1
    
        ret = 0
        x = iMax
        a = (sum0+sum2)/2.0 - sum1
        b = sum2 - sum1 - 2.0*a*x - a
        if(abs(a) < 1.0e-18):
            ret = iMax
        else:
            ret = (-b/(2.0*a))
        return ret
 
    def FitGaussian(self, rawData):
        def gauss(x, *p):
            A, mu, sigma = p
            return A*np.exp(-(x-mu)**2/(2.0*sigma**2))
    
        # Smooth the raw data and find the raw data estimated peak maximum
        smoothedRaw = scipy.signal.medfilt(rawData,31)
        maxInt = np.amax(smoothedRaw)
        maxIndex = np.where(smoothedRaw == np.amax(smoothedRaw))
        maxIndex = np.average(maxIndex)
        maxIndex = int(maxIndex)        
            
        x = np.arange(0, len(rawData))
        y_to_fit = np.copy(rawData)
    
        # The peak is skewed so concentrate the gaussian fit around the
        # peak maximum.  The overall fit won't look good but this might
        # serve as a good sub-pixel peak center estimator for
        # a cross-correlation fit.
        hw = 50
        x_sub = x[maxIndex-hw:maxIndex+hw]
        y_sub = y_to_fit[maxIndex-hw:maxIndex+hw]
            
        p_initial = [maxInt, maxIndex, 10.0]
    
        coeff, var_matrix = curve_fit(gauss, x_sub, y_sub, p_initial)
    
        y_fitted = gauss(x, *coeff)
        residual = rawData - y_fitted
        residual = residual*10
        residual = residual + 0.5
        residual = residual[0:2500]
        
        # plt.plot(y_to_fit)
        # plt.plot(y_fitted)
        # plt.show()
    
        return y_to_fit, y_fitted, residual, coeff

import sys

if __name__ == "__main__":
    ff = FFCCFitter()
    (time, nm) = ff.getSensorgram(sys.argv[1])
    plt.plot(time, nm)
    plt.xlabel("Time [s]")
    plt.ylabel("Shift [nm]")
    plt.show()

