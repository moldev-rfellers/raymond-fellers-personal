# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 18:34:32 2020

Class to feed spectra files to a fitter and stitch sensorgrams.

@author: ForteBio
"""

import numpy as np
import OctetUtilities.Filters as flt
import OctetFitter.FFCCFitter as ffcc
import matplotlib.pyplot as plt

class AssayFitter():
    def __init__(self, manifestFile):
        self.manifestFile = manifestFile
        self.spectraFiles = []
        self.parseManifestFile()
        self.fitSpectra()
        return
    
    def parseManifestFile(self):
        # Read the manifest looking for spectra files.
        with open(self.manifestFile) as f:
            for line in f:
                line = line.strip()
                if(line.endswith("spectra") and not line.startswith('#')):
                    self.spectraFiles.append(line)
        return
                        
                        
    def fitSpectra(self):
        assayTime = []
        assayNm = []
        assayNmSm = []
        for file in self.spectraFiles:
            print("Processing spectra file ", file)
            ff = ffcc.FFCCFitter()
            (time, nm) = ff.getSensorgram(file)     
            (assayNm, assayNmSm, assayTime) = self.smoothAppend(assayNm, nm, assayTime, time)

        plt.plot(assayTime, assayNm)
        plt.plot(assayTime, assayNmSm)
        return
        
    # Append array2 to array1 and return the new combined array.
    # If array1 is empty or None, return array2.
    #
    # We assume here that if array1 and array2 are data from the same assay,
    # there should be a smooth transition between the two.  Assay2 may have
    # a bias relative to assay1 due to a difference in well conditions.
    # To compensate for this bias we smooth both arrays and determine
    # the offset between the two.  The we apply an offset correction to array2
    # to effect a smooth transition.
    #
    def smoothAppend(self, array1, array2, time1, time2):
        # Smoothing func only works on numpy arrays
        a1 = np.array(array1)
        a2 = np.array(array2)
        t1 = np.array(time1)
        t2 = np.array(time2)
        
        # Handle first time through.
        if(a1.size < 1):
            return(a2.tolist(), a2.tolist(), t2.tolist())
        
        # Make sure the smoothing windows isn't wider than the input data.
        # The window of 31 arbritrary, makes the plots look "nice".
        minLength = min(a1.size, a2.size)
        wl = 31
        if(minLength < wl):
            wl = minLength
            
        sm_a1 = flt.octet_smooth(a1, window_len=wl)
        sm_a2 = flt.octet_smooth(a2, window_len=wl)
        
        # Find and apply the necessary offset
        delta = abs(sm_a1[-1] - sm_a2[0])
        if(sm_a1[-1] > sm_a2[0]):
            sm_a2 = sm_a2 + delta
            a2 = a2 + delta
        else:
            sm_a2 = sm_a2 - delta
            a2 = a2 - delta
        sm_a12 = np.append(sm_a1, sm_a2)
        a12 = np.append(a1, a2)
        
        dt = t1[-1] - t1[-2]
        t2 = t2 + (dt + t1[-1])
        t12 = np.append(t1, t2)
            
        return (a12.tolist(), sm_a12.tolist(), t12.tolist())

import sys       
if __name__ == "__main__":
    af = AssayFitter(sys.argv[1])