# -*- coding: utf-8 -*-

import numpy as np

class FFCCMatrix():
    def __init__(self):
        self.maxFrames = 5
        self.matDict = {}
        self.createMatricies()
        
    def createMatricies(self):    
        for matId in range(5,32):
            maxCols = matId    
            B = np.zeros((0,maxCols))            
            for col in range(1, maxCols+1):
                for frame in range(1, self.maxFrames+1):
                    if(frame <= col):
                        startCol = col-1
                        a = np.zeros(maxCols)
                        for i in reversed(range(1,frame+1)):
                            a[startCol] = 1/frame
                            startCol = startCol - 1
                        B = np.vstack((B, a))
            
            self.matDict[matId] = np.linalg.inv(B.transpose() @ B) @ B.transpose()

    def getRow(self, matId, rowId):
        return self.matDict[matId][rowId]
    
if __name__ == "__main__":
    ffmat = FFCCMatrix()
    print(ffmat.getRow(5,0))