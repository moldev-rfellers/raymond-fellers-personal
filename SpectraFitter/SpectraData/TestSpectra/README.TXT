Spectra files for development, testing, and demos.

1. S01_C09.spectra: Quant. spectra used for the Jupyter doc. explaining how
the Five Frame Cross-Correlation algorithm works.  This spectra is a loading
step with a 4nm shift.

2. S02_C08 through S13_C08: Furosemide, small nm shift, strong baseline slope.
Contains 4 on/off steps with increasing concentration.

