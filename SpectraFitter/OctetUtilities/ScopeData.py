# Unpack the Octet binary spectra files into a Python nested list.
# The binary files have the names like S01_C01.spectra and contain
# the raw scope data.  To get the transmission you also need to
# dark current and reference spectra files.
#
# The binary files consist of a header of UINTS and a series of spectra
# recorded over time.  Each series start with a UINT, which is the
# relative timestamp in ms, followed by (usually) 3648 FLOATS, which are
# light intensities for each pixel of the spectrometer CCD.
#
# Spectra Header format, 68 bytes
# UINT = Signature
# UINT = File Version
# UINT = Data Length (number of pixels per spectra)
# UINT = Spectra Bytes (per spectra)
# UINT = Date
# UINT = Time
# UINT = Control Version
# BYTE = Unused 40 element byte array
#
# One spectra
# UINT  = Relative time in ms, first read is 0 ms
# FLOAT = Number of floats is 'data length' from the header
#

from struct import Struct

class ScopeData():
    def __init__(self):
        pass

    def helloFromScopeData(self):
        print("Hello from scope data")

    # Read in the spectra and put it in a nested array.
    # The input is the pathname to a specta file.
    # Each element of the output array contains this kind of array
    #
    #   [ time, [ spectra: array of floats ] ]
    #
    # scopeDataCollection[3][0] = time of 4th spectra
    # scopeDataCollection[3][1] = array of floats which are the 3rd spectra
    # scopeDataCollection[3][1][4] = 5th pixel of the 4th spectra
    #
    def getDataFromScopeBinaryFile(self, scopeBinaryPathName):
        scopeDataCollection = []
        with open(scopeBinaryPathName, "rb") as binary_file:

            # Read the header data
            headerStruct = Struct('17I')
            data = binary_file.read(headerStruct.size)
            myInts = headerStruct.unpack(data)
            numPixels = myInts[2]
       
            while True:
                timeStruct = Struct('I')
                scopeDataStruct = Struct(str(numPixels) + 'f')

                # Each spectra starts with the relative time in ms
                data = binary_file.read(timeStruct.size)
                if data == b'':
                    break # No more spectra to read in
                timeMS = timeStruct.unpack(data)

                # Now read in the pixel intensities
                data = binary_file.read(scopeDataStruct.size)
                scopeData = scopeDataStruct.unpack(data)

                scopeDataCollection.append([timeMS[0], scopeData])
                
        return scopeDataCollection

import sys
if __name__ == "__main__":
    sd = ScopeData()
    scope = sd.getDataFromScopeBinaryFile(sys.argv[1])
    numSpectra = len(scope)-1
    print("Data of last spectra in the file:")
    print("Time [s]:", (scope[numSpectra][0])/1000)
    print("4th pixel:", scope[numSpectra][1][3])
    
