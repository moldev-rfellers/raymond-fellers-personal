# Transmission
#
# Gather the dark, reference, and scope data to make transmission
# data for cross-correlation fitting.
#
import re
import os
import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from OctetUtilities.ScopeData import ScopeData

class TransmissionData():
    def __init__(self):
        self.wavelengths = []
        f = interp1d
        pass

    def getWavelength(self, idx):
        return self.f(idx)
    
    # def getPixelToNanoMeter(self, maxIdx):
    #     p2nm = 0.0
    #     shiftFactor = (self.wavelengths[-1] - self.wavelengths[0])/len(self.wavelengths)
    #     p2nm = shiftFactor*(self.wavelengths[maxIdx]/(shiftFactor*maxIdx + self.wavelengths[0]))
    #     print("sf:",shiftFactor)
    #     return p2nm
        
    # Transmission is
    #   t = (scope - darkCurrent)/(reference - darkCurrent)
    #
    # t is the data that we fit with cross-correlation to get an
    # Octet spectrogram.
    #
    def makeTransmissionDataFromSpectraFile(self, scopeBinaryPathName):
        sd = ScopeData()
        scope = sd.getDataFromScopeBinaryFile(scopeBinaryPathName)
        (darkFileName, refFileName) = self.getDarkRefFileNames(scopeBinaryPathName)
        (wavelengths, dark) = self.getDarkRefData(darkFileName)
        (_, ref) = self.getDarkRefData(refFileName)
        self.wavelengths = wavelengths
        
        # Create a wavelength interpolation function so we can
        # estimate the wavelength for sub-pixel resolution.
        self.f = interp1d(range(len(wavelengths)), wavelengths)

        transmission = []
        for j in range(len(scope)):
            time = np.copy(scope[j][0])
            spectra = np.copy(scope[j][1])
            t = np.empty([len(spectra)])
            for i in range(len(spectra)):
                t[i] = ((spectra[i] - dark[i]) / (ref[i] - dark[i]))
            transmission.append([time, t])
        return (np.asarray(transmission), wavelengths)

    # Get the channel number from the spectra file name and make
    # the dark current and ref spectra file names.
    # The channel is ## in S01_C##.spectra.
    # str(int()) gets rid of the leading zeros.
    def getDarkRefFileNames(self, scopeBinaryPathName):
        chList = re.findall(r'C(\d{2})\.', scopeBinaryPathName)
        if(not chList):
            print("Can't get channel # in getDarkRefFileNames")
            return ""
        ch = str(int(chList[0]))
        datDir = os.path.dirname(scopeBinaryPathName)
        dark = os.path.join(datDir, "Dark" + ch + ".txt")
        ref = os.path.join(datDir, "Ref" + ch + ".txt")
        return (dark, ref)

    # Read in dark current or ref spectra file.
    # Both have the same format:
    # Header
    #   File pathname
    #   Version info
    #   Spectrometer SN
    #   String "Data<<<<<" signaling start of data follows
    # Data (usually 3648 lines)
    #   wavelength <space> intensity
    #
    def getDarkRefData(self, fileName):
        wavelengths = []
        intensity = []
        with open(fileName, 'r') as fn:
            finishedHeader = False
            for line in fn:
                if line.startswith('Data<<<'):
                    finishedHeader = True
                    continue
                elif finishedHeader:
                    data = line.split()
                    wavelengths.append(float(data[0]))
                    intensity.append(float(data[1]))
        return (wavelengths, intensity)  


import site
import sys
site.addsitedir("C:\\Users\\ForteBio\\Documents\\BitBucket\\rd\\SpectraFitter\\")
import scipy.signal
if __name__ == "__main__":
    td = TransmissionData()
    transmission = td.makeTransmissionDataFromSpectraFile(sys.argv[1])
    raw = transmission[0][1]
    smoothed = scipy.signal.savgol_filter(raw, 51, 3)
    plt.plot(raw)
    plt.plot(smoothed)
    plt.show()
    
